package app.dao;

import app.model.Clinic;
import app.model.User;

import java.sql.*;

/**
 * Created by wesleyguirra on 5/13/17.
 */
public class ClinicDao extends AbstractDao {

    private static final String FIND_BY_CNPJ = "SELECT * FROM clinics WHERE cnpj=?";
    private static final String INSERT = "INSERT INTO clinics(role, cnpj, name, password, email, address) VALUES(?, ?, ?, ?, ?, ?)";

    public Boolean insert(Clinic clinic) {
        Connection conn = null;
        PreparedStatement stmt = null;
        System.out.println(clinic.getRole());
        try {
            conn = getConnection();
            stmt = conn.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, clinic.getRole());
            stmt.setString(2, clinic.getCnpj());
            stmt.setString(3, clinic.getName());
            stmt.setString(4, clinic.getPassword());
            stmt.setString(5, clinic.getEmail());
            stmt.setString(6, clinic.getAddress());

            int result = stmt.executeUpdate();
            ResultSet rs = stmt.getGeneratedKeys();

            return true;
        } catch (SQLException e) {
            // e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            close(stmt);
            close(conn);
        }
    }

    public Clinic findByCnpj(String cnpj) {
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            conn = getConnection();
            stmt = conn.prepareStatement(FIND_BY_CNPJ);
            stmt.setString(1, cnpj);

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                Clinic clinic = new Clinic();
                clinic.setId(rs.getInt("id"));
                clinic.setRole(rs.getString("role"));
                clinic.setCnpj(rs.getString("cnpj"));
                clinic.setName(rs.getString("name"));
                clinic.setEmail(rs.getString("email"));
                clinic.setPassword(rs.getString("password"));
                clinic.setAddress(rs.getString("address"));

                return clinic;
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            close(stmt);
            close(conn);
        }
    }
}
