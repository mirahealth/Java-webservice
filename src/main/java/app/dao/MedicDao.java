package app.dao;

import app.model.Clinic;
import app.model.Medic;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wesleyguirra on 5/13/17.
 */
public class MedicDao extends AbstractDao {
    private static final String FIND_BY_ID = "SELECT * FROM doctors WHERE id=?";
    private static final String FIND_ALL = "SELECT * FROM doctors ORDER BY name";
    private static final String FIND_BY_CPS = "SELECT * FROM doctors WHERE cps=?";
    private static final String INSERT = "INSERT INTO doctors(role, cps, name, password, address) VALUES(?, ?, ?, ?, ?)";

    public List<Medic> findAll(){
        Connection conn = null;
        PreparedStatement stmt = null;
        List<Medic> list = new ArrayList<Medic>();

        try {
            conn = getConnection();
            stmt = conn.prepareStatement(FIND_ALL);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Medic medic = new Medic();
                medic.setId(rs.getInt("id"));
                medic.setRole(rs.getString("role"));
                medic.setCps(rs.getString("cps"));
                medic.setName(rs.getString("name"));
                medic.setPassword(rs.getString("password"));
                medic.setAddress(rs.getString("address"));

                list.add(medic);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            close(stmt);
            close(conn);
        }

        return list;

    }

    public Boolean insert(Medic medic) {
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            conn = getConnection();
            stmt = conn.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, medic.getRole());
            stmt.setString(2, medic.getCps());
            stmt.setString(3, medic.getName());
            stmt.setString(4, medic.getPassword());
            stmt.setString(5, medic.getAddress());

            int result = stmt.executeUpdate();
            ResultSet rs = stmt.getGeneratedKeys();

            return true;
        } catch (SQLException e) {
            // e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            close(stmt);
            close(conn);
        }
    }

    public Medic findByCps(String cps) {
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            conn = getConnection();
            stmt = conn.prepareStatement(FIND_BY_CPS);
            stmt.setString(1, cps);

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                Medic medic = new Medic();
                medic.setId(rs.getInt("id"));
                medic.setRole(rs.getString("role"));
                medic.setCps(rs.getString("cps"));
                medic.setName(rs.getString("name"));
                medic.setPassword(rs.getString("password"));
                medic.setAddress(rs.getString("address"));

                return medic;
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            close(stmt);
            close(conn);
        }
    }

    public Medic findMedicById(int id) {
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            conn = getConnection();
            stmt = conn.prepareStatement(FIND_BY_ID);
            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                Medic medic = new Medic();
                medic.setId(rs.getInt("id"));
                medic.setRole(rs.getString("role"));
                medic.setCps(rs.getString("cps"));
                medic.setName(rs.getString("name"));
                medic.setAddress(rs.getString("address"));

                return medic;
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            close(stmt);
            close(conn);
        }
    }

}
