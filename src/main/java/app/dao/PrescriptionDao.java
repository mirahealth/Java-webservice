package app.dao;

import app.model.Medic;
import app.model.Prescription;

import java.sql.*;
import java.sql.Date;
import java.util.*;

/**
 * Created by wesleyguirra on 5/24/17.
 */
public class PrescriptionDao extends AbstractDao {
    private static final String FIND_ALL = "SELECT * FROM prescriptions ORDER BY date";
    private static final String FIND_BY_ID = "SELECT * FROM prescriptions WHERE id=?";
    private static final String FIND_BY_DATE = "SELECT * FROM prescriptions WHERE date=?";
    private static final String FIND_BY_MEDIC = "SELECT * FROM prescriptions WHERE medic=?";
    private static final String FIND_BY_PATIENT = "SELECT * FROM prescriptions WHERE patient=?";
    private static final String INSERT = "INSERT INTO prescriptions(name, date, medic, patient, cid, recommendations, status) VALUES(?, ?, ?, ?, ?, ?, ?)";
    private static final String DELETE_BY_ID = "UPDATE prescriptions SET status=0 WHERE id=?";

    public List<Prescription> findAll(){
        Connection conn = null;
        PreparedStatement stmt = null;
        List<Prescription> list = new ArrayList<Prescription>();

        try {
            conn = getConnection();
            stmt = conn.prepareStatement(FIND_ALL);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Prescription prescription = new Prescription();
                prescription.setId(rs.getInt("id"));
                prescription.setName(rs.getString("name"));
                prescription.setDate(rs.getDate("date"));
                prescription.setMedic(rs.getString("medic"));
                prescription.setPatient(rs.getString("patient"));
                prescription.setCid(rs.getString("cid"));
                prescription.setRecommendations(rs.getString("recommendations"));
                list.add(prescription);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            close(stmt);
            close(conn);
        }

        return list;

    }

    public int insert(Prescription prescription) {
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            conn = getConnection();
            stmt = conn.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, prescription.getName());
            stmt.setDate(2, prescription.getDate());
            stmt.setString(3, prescription.getMedic());
            stmt.setString(4, prescription.getPatient());
            stmt.setString(5, prescription.getCid());
            stmt.setString(6, prescription.getRecommendations());
            stmt.setInt(7, 1);

            int result = stmt.executeUpdate();
            ResultSet rs = stmt.getGeneratedKeys();

            if (rs.next()) {
            }

            return result;
        } catch (SQLException e) {
            // e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            close(stmt);
            close(conn);
        }
    }

    public List<Prescription> findByDate(Date date) {
        Connection conn = null;
        PreparedStatement stmt = null;
        List<Prescription> list = new ArrayList<Prescription>();

        try {
            conn = getConnection();
            stmt = conn.prepareStatement(FIND_BY_DATE);
            stmt.setDate(1, date);

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Prescription prescription = new Prescription();
                prescription.setId(rs.getInt("id"));
                prescription.setName(rs.getString("name"));
                prescription.setMedic(rs.getString("medic"));
                prescription.setPatient(rs.getString("patient"));
                prescription.setCid(rs.getString("cid"));
                prescription.setRecommendations(rs.getString("recommendations"));
                prescription.setDate(rs.getDate("date"));
                prescription.setStatus(rs.getInt("status"));
                list.add(prescription);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            close(stmt);
            close(conn);
        }
        return list;
    }

    public Prescription findByMedicId(int medicId) {
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            conn = getConnection();
            stmt = conn.prepareStatement(FIND_BY_MEDIC);
            stmt.setInt(1, medicId);

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                Prescription prescription = new Prescription();
                prescription.setId(rs.getInt("id"));
                prescription.setName(rs.getString("name"));
                prescription.setMedic(rs.getString("medic"));
                prescription.setPatient(rs.getString("patient"));
                prescription.setCid(rs.getString("cid"));
                prescription.setRecommendations(rs.getString("recommendations"));
                prescription.setDate(rs.getDate("date"));
                prescription.setStatus(rs.getInt("status"));


                return prescription;
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            close(stmt);
            close(conn);
        }
    }

    public Prescription findById(int id) {
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            conn = getConnection();
            stmt = conn.prepareStatement(FIND_BY_ID);
            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                Prescription prescription = new Prescription();
                prescription.setId(rs.getInt("id"));
                prescription.setName(rs.getString("name"));
                prescription.setMedic(rs.getString("medic"));
                prescription.setPatient(rs.getString("patient"));
                prescription.setCid(rs.getString("cid"));
                prescription.setRecommendations(rs.getString("recommendations"));
                prescription.setDate(rs.getDate("date"));
                prescription.setStatus(rs.getInt("status"));


                return prescription;
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            close(stmt);
            close(conn);
        }
    }

    public Prescription findByPatient(int patientId) {
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            conn = getConnection();
            stmt = conn.prepareStatement(FIND_BY_PATIENT);
            stmt.setInt(1, patientId);

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                Prescription prescription = new Prescription();
                prescription.setId(rs.getInt("id"));
                prescription.setName(rs.getString("name"));
                prescription.setMedic(rs.getString("medic"));
                prescription.setPatient(rs.getString("patient"));
                prescription.setCid(rs.getString("cid"));
                prescription.setRecommendations(rs.getString("recommendations"));
                prescription.setDate(rs.getDate("date"));
                prescription.setStatus(rs.getInt("status"));


                return prescription;
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            close(stmt);
            close(conn);
        }
    }
}
