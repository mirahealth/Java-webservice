package app.dao;

import app.model.Clinic;
import app.model.Patient;

import java.sql.*;

/**
 * Created by wesleyguirra on 5/13/17.
 */
public class PatientDao extends AbstractDao {

    private static final String FIND_BY_CPF = "SELECT * FROM patients WHERE cpf=?";
    private static final String INSERT = "INSERT INTO patients(role, cpf, name, address) VALUES(?, ?, ?, ?)";

    public Boolean insert(Patient patient) {
        Connection conn = null;
        PreparedStatement stmt = null;
        System.out.println(patient.getRole());
        try {
            conn = getConnection();
            stmt = conn.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, patient.getRole());
            stmt.setString(2, patient.getCpf());
            stmt.setString(3, patient.getName());
            stmt.setString(4, patient.getAddress());

            int result = stmt.executeUpdate();
            ResultSet rs = stmt.getGeneratedKeys();

            return true;
        } catch (SQLException e) {
            // e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            close(stmt);
            close(conn);
        }
    }

    public Patient findByCpf(String cpf) {
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            conn = getConnection();
            stmt = conn.prepareStatement(FIND_BY_CPF);
            stmt.setString(1, cpf);

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                Patient patient = new Patient();
                patient.setId(rs.getInt("id"));
                patient.setRole(rs.getString("role"));
                patient.setCpf(rs.getString("cpf"));
                patient.setName(rs.getString("name"));
                patient.setAddress(rs.getString("address"));
                return patient;
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            close(stmt);
            close(conn);
        }
    }
}
