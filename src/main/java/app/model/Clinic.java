package app.model;

import app.dao.ClinicDao;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by wesleyguirra on 5/13/17.
 */
public class Clinic extends User {
    private String cnpj;
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public Boolean canLogin(String password) {

        return password.equals(this.getPassword());

    }

    private Boolean canRegister(String cnpj){

        ClinicDao clinicDao = new ClinicDao();
        // Check if the user exists by searching for his cnpj
        if(clinicDao.findByCnpj(cnpj) != null){
            return false;
        } else {
            return true;
        }
    }

    public String getIntegrationToken() throws UnsupportedEncodingException {

        Map<String, Object> claims = new HashMap();
        claims.put("cnpj", this.getCnpj());
        claims.put("role", this.getRole());
        claims.put("id", this.getId());
        claims.put("email", this.getEmail());
        claims.put("name", this.getName());
        claims.put("usertype",this.getUsertype());
        JwtBuilder builder = Jwts.builder()
                .setSubject("Teste")
                .setClaims(claims)
                .setIssuer("teste")
                .signWith(SignatureAlgorithm.HS512, "hmira");
        return builder.compact();
    }
}
