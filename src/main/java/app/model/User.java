package app.model;

import java.io.UnsupportedEncodingException;

/**
 * Created by wesleyguirra on 4/20/17.
 */
public abstract class User {
    private long id;
    private String name;
    private String usertype;
    private String password;
    private String role;
    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean canLogin(String password) {
        return password.equals(this.getPassword());
    }

    public abstract String getIntegrationToken() throws UnsupportedEncodingException;


}
