package app.model;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by wesleyguirra on 5/13/17.
 */
public class Medic extends User {
    //Código do profissional de saúde
    private String cps;

    public String getCps() {
        return cps;
    }

    public void setCps(String cps) {
        this.cps = cps;
    }


    public String getIntegrationToken() throws UnsupportedEncodingException {

        Map<String, Object> claims = new HashMap();
        claims.put("cps", this.getCps());
        claims.put("id", this.getId());
        claims.put("name", this.getName());
        claims.put("role", this.getRole());
        JwtBuilder builder = Jwts.builder()
                .setClaims(claims)
                .setIssuer("teste")
                .signWith(SignatureAlgorithm.HS512, "hmira");
        return builder.compact();
    }

    public boolean validate(String token) {

        try {
            Jwts.parser().setSigningKey("hmira").parseClaimsJws(token);
            return true;
        } catch (SignatureException e){
            //UTF-8 encoding not supported
            return false;
        }
    }
}
