package app.model;

/**
 * Created by wesleyguirra on 5/24/17.
 */

import app.dao.PatientDao;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class Patient extends User {
    private String cpf;

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    private Boolean canRegister(String cpf){

        PatientDao patientDao = new PatientDao();
        // Check if the user exists by searching for his cnpj
        if(patientDao.findByCpf(cpf) != null){
            return false;
        } else {
            return true;
        }
    }

    public String getIntegrationToken() throws UnsupportedEncodingException {

        Map<String, Object> claims = new HashMap();
        claims.put("cpf", this.getCpf());
        claims.put("role", this.getRole());
        claims.put("id", this.getId());
        claims.put("name", this.getName());
        claims.put("usertype",this.getUsertype());
        JwtBuilder builder = Jwts.builder()
                .setClaims(claims)
                .setIssuer("teste")
                .signWith(SignatureAlgorithm.HS512, "hmira");
        return builder.compact();
    }
}
