package app.service;


import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;

import java.io.UnsupportedEncodingException;

/**
 * Created by wesleyguirra on 5/24/17.
 */
public class TokenValidator {
    public static boolean validate(String token) {

        try {
            Jwts.parser().setSigningKey("hmira").parseClaimsJws(token);
            return true;
        } catch (SignatureException e){
            //UTF-8 encoding not supported
            return false;
        }
    }
}
