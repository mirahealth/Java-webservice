package app;

import app.controller.*;
import app.service.TokenValidator;
import static spark.Spark.*;

/**
 * Created by wesleyguirra on 4/23/17.
 */
public class Application {
    // Enables CORS on requests. This method is an initialization method and should be called once.

    private static void enableCORS(final String origin, final String methods, final String headers) {

        options("/*", (request, response) -> {

            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }

            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }

            return "OK";
        });

        before((request, response) -> {
            response.header("Access-Control-Allow-Origin", origin);
            response.header("Access-Control-Request-Method", methods);
            response.header("Access-Control-Allow-Headers", headers);
            // Note: this may or may not be necessary in your particular application
            response.type("application/json");
        });
    }
    public static void main(String[] args) {
        before("api/v1/protected/*", ((request, response) -> {

            String token = request.headers("Authorization");

            System.out.println(token);

            if (token != null) {
                boolean authenticated = TokenValidator.validate(token);

                if (!authenticated) {
                    halt(401, "token expired");
                }
            } else {
                halt(401, "header token not found");
            }

        }));

        enableCORS("*","GET, PUT, POST, DELETE, OPTIONS", "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin,");
        //Map<String, Object> model = new HashMap<>();

        String API_CONTEXT = "/api/v1";
        String API_PROTECTED = "/api/v1/protected";

        //Paths endpoints
        get(API_CONTEXT + "/prescription", (request, response) -> PrescriptionController.listAllPrescriptions(request));
        get(API_CONTEXT + "/prescription/:date", (request, response) -> PrescriptionController.listByDate(request));

        // List Users
        get(API_CONTEXT + "/medic", (request, response) -> UserController.listAllMedics(request));
        get(API_CONTEXT + "/medic/:id", (request, response) -> UserController.listMedicById(request));

        post(API_CONTEXT + "/clinic", ((request, response) -> UserController.startRegister(request, response)));
        post(API_CONTEXT + "/user", ((request, response) -> UserController.startRegister(request, response)));
        get(API_CONTEXT + "/me", ((request, response) -> UserController.getActualUser(request, response)));

        //Login paths
        post(API_CONTEXT + "/login", ((request, response) -> UserController.startLogin(request, response)));

        // post(API_CONTEXT + "/sign-in", (request, response) -> LoginController.startLogin(request, response));

    }

}
