package app.controller;

import app.dao.ClinicDao;
import app.dao.MedicDao;
import app.dao.PatientDao;
import app.model.Clinic;
import app.model.Medic;
import app.model.Patient;
import com.google.gson.Gson;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import spark.Request;
import spark.Response;

import java.io.UnsupportedEncodingException;
import java.util.List;

import static java.lang.Integer.parseInt;
import static spark.Spark.halt;

/**
 * Created by wesleyguirra on 5/28/17.
 */
public class UserController {
    // Register
    // Login

    public static String getActualUser(Request request, Response response) {
        String token = request.headers("Authorization");
        Claims claims = Jwts.parser().setSigningKey("hmira").parseClaimsJws(token).getBody();
        Gson gson = new Gson();

        return gson.toJson(claims);
    }

    public static String startRegister(Request request, Response response) throws UnsupportedEncodingException {

        String userType = request.headers("usertype");

        if (userType.equals("medico")){

            Gson gson = new Gson();
            Medic medic = gson.fromJson(request.body(), Medic.class);
            return registerMedic(medic, response);

        } else if(userType.equals("patient")) {
            System.out.println(request.body());
            Gson gson = new Gson();
            Patient patient = gson.fromJson(request.body(), Patient.class);
            return registerPatient(patient, response);

        } else if(userType.equals("clinica")) {

            Gson gson = new Gson();
            Clinic clinic = gson.fromJson(request.body(), Clinic.class);
            return registerClinic(clinic, response);

        } else {
            return null;
        }
    }
    private static String registerMedic (Medic medic, Response response) throws UnsupportedEncodingException {

        MedicDao medicDao = new MedicDao();

        Medic medicFound = medicDao.findByCps(medic.getCps());

        if (medicFound != null){
            response.type("application/json");
            halt(400, "CPS já cadastrado");
            return null;
        } else {
            if (medicDao.insert(medic)) {
                response.type("application/json");
                response.status(200);
                return "OK";
            } else {
                response.type("application/json");
                halt(500, "Erro interno");
                return " ";
            }
        }
    }

    private static String registerClinic (Clinic clinic, Response response) throws UnsupportedEncodingException {

        ClinicDao clinicDao = new ClinicDao();

        Clinic clinicFound = clinicDao.findByCnpj(clinic.getCnpj());

        if (clinicFound != null){
            response.type("application/json");
            halt(400, "{\"message\":\"CNPJ já cadastrado!\"}");
            return null;
        } else {
            if (clinicDao.insert(clinic)) {
                response.type("application/json");
                halt(200, "{\"message\":\"Cadastro realizado com sucesso!\"}");
                return null;
            } else {
                response.type("application/json");
                halt(500, "{\"message\":\"Aconteceu um erro em nosso servidor, tente em alguns instantes.\"}");
                return null;
            }

        }
    }

    private static String registerPatient (Patient patient, Response response) throws UnsupportedEncodingException {

        PatientDao patientDao = new PatientDao();

        Patient patientFound = patientDao.findByCpf(patient.getCpf());

        if (patientFound != null){
            response.type("application/json");
            halt(400, "{\"message\":\"CPF já cadastrado!\"}");
            return null;
        } else {
            if (patientDao.insert(patient)) {
                response.type("application/json");
                halt(200, "{\"message\":\"Cadastro realizado com sucesso!\"}");
                return null;
            } else {
                response.type("application/json");
                halt(500, "{\"message\":\"Aconteceu um erro em nosso servidor, tente em alguns instantes.\"}");
                return null;
            }

        }
    }
    // Login
    public static String startLogin(Request request, Response response) throws UnsupportedEncodingException {

        String userType = request.headers("usertype");

        if (userType.equals("clinica")){
            Gson gson = new Gson();

            Clinic clinic = gson.fromJson(request.body(), Clinic.class);
            return loginClinic(clinic, response);
        } else if(userType.equals("medico")) {
            Gson gson = new Gson();
            Medic medic = gson.fromJson(request.body(), Medic.class);
            return loginMedic(medic, response);
        } else {
            return null;
        }
    }


    private static String loginMedic(Medic medic, Response response) throws UnsupportedEncodingException{

        MedicDao medicDao = new MedicDao();

        Medic medicUser = medicDao.findByCps(medic.getCps());

        if (medicUser != null){
            if(medicUser.canLogin(medic.getPassword())){
                response.type("application/json");
                response.status(200);
                return "{\"token\":\"" + medicUser.getIntegrationToken() + "\", \"message\":\"Logado com sucesso!\"}";
            } else {
                response.type("application/json");
                halt(400, "{\"message\":\"Senha incorreta!\"}");
                return null;
            }
        } else {
            response.type("application/json");
            halt(404, "{\"message\":\"Usuário não encontrado!\"}");
            return null;
        }

    }

    private static String loginClinic(Clinic clinic, Response response) throws UnsupportedEncodingException{

        ClinicDao clinicDao = new ClinicDao();

        Clinic clinicUser = clinicDao.findByCnpj(clinic.getCnpj());

        if (clinicUser != null){
            if(clinicUser.canLogin(clinic.getPassword())){
                response.type("application/json");
                response.status(200);
                return "{\"token\":\"" + clinicUser.getIntegrationToken() + "\", \"message\":\"Logado com sucesso!\"}";
            } else {
                response.type("application/json");
                halt(400, "{\"message\":\"Senha incorreta!\"}");
                return null;
            }
        } else {
            response.type("application/json");
            halt(404, "{\"message\":\"Usuário não encontrado!\"}");
            return null;
        }
    }
    // List
    public static String listAllMedics(Request request) {

        MedicDao medicDao = new MedicDao();

        List users = medicDao.findAll();

        Gson gson = new Gson();

        return gson.toJson(users);

    }

    public static String listMedicById(Request request) {
        MedicDao medicDao = new MedicDao();

        Medic medic = medicDao.findMedicById(parseInt(request.params(":id")));

        Gson gson = new Gson();

        return gson.toJson(medic);
    }
}
