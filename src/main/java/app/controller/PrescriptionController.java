package app.controller;

import app.dao.MedicDao;
import app.dao.PrescriptionDao;
import app.model.Medic;
import app.model.Prescription;
import com.google.gson.Gson;
import spark.Request;

import java.sql.Date;
import java.util.List;

import static java.lang.Integer.parseInt;

/**
 * Created by wesleyguirra on 5/28/17.
 */
public class PrescriptionController {
    // List
    public static String listAllPrescriptions(Request request) {

        PrescriptionDao prescriptionDao = new PrescriptionDao();

        List prescriptions = prescriptionDao.findAll();

        Gson gson = new Gson();

        return gson.toJson(prescriptions);

    }

    public static String listByDate(Request request) {
        PrescriptionDao prescriptionDao = new PrescriptionDao();

        List prescriptions = prescriptionDao.findByDate(Date.valueOf(request.params(":date")));

        Gson gson = new Gson();

        return gson.toJson(prescriptions);
    }
}
